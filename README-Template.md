# TruckO

Simple application written UWP C# allowing a person to view TruckersMP servers status Xbox One or Windows 10 PC

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 
to install goto Visual Studio 2017 folder click on Visual Studio installer then select the You can skip these step if you've already done this.


## License

This project is licensed under the GNU GPLv3​ License - see the [LICENSE.md](LICENSE.md) file for details


## Authors
* Robert Foster aka Techwhipped [TruckersMP Profile](https://truckersmp.com/user/910291)

## Changelogs
* Version 1.1.2.0 - Initial Release 

## Beta Release
* [Microsoft Store Link](https://www.microsoft.com/en-us/p/trucko/9nkcgb6gwlk9?rtc=1&activetab=pivot%3Aoverviewtab%7Cpivot%3Aregionofsystemrequirementstab)

## Requirements

* If testing on Xbox One you'll need a developer license the cost of this is $25.00
* Visual Studio 2017
* Universal Window App Developement Tools


