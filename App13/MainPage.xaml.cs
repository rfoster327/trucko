﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.UI.Xaml;
using Newtonsoft.Json.Linq;
using Windows.UI.Xaml.Controls;
using System.Net;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace App13
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private int counter = 0;
        private DispatcherTimer timer;
        
        public MainPage()
        {
            this.InitializeComponent();
            timer = new DispatcherTimer();
           //lets load servers ID on start of app
            servers.Items.Add("1"); // Europe 1 [Simulation] Server ID - ETS2
            servers.Items.Add("4"); // Europe 2 Server ID - ETS 2
            servers.Items.Add("5"); // Europe 3 [No Cars] Server ID - ETS 2
            servers.Items.Add("7"); // South American Server ID - ETS 2`
            servers.Items.Add("8"); // Europe 4 [Freeroam] Server ID - ETS 2
            servers.Items.Add("9"); // Europe 2 Server ID Game - ATS
            servers.Items.Add("11"); // United States Server ID - ATS`

            servers.SelectedIndex = 7; // Selects United States servers on load
        }


        private void Timer_Tick(object sender, object e)
        {
            if (counter == 120)
            {
              

                //Button_Start.Click += new RoutedEventHandler(Button_Click);
               
                Button_Click(null, null); // I'm pretty sure we know what button_click does
        
                servers_SelectionChanged(null, null); // server selection call
                list.Items.Clear(); // pretty sure we know what items.clear does
                counter = 0; // pretty sure you know this rest the timer back to 0 keeping us from spamming api servers.
                // textme.Text = counter.ToString();
                //if you want to stop the timer use timer.Start()
            }
            else
            {
                counter++;
            }




        }
        private async void Button_Click(object sender, RoutedEventArgs e)
        {


 
            StreamReader reader = default(StreamReader);
           
            string url = "https://api.truckersmp.com/v2/servers";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            HttpWebResponse response = (HttpWebResponse)await request.GetResponseAsync(); // (note: await added, method call changed to GetResponseAsync()

            reader = new StreamReader(response.GetResponseStream());
            string responseFromServer = reader.ReadToEnd();
            string json = responseFromServer;
            string rawresp;
            rawresp = reader.ReadToEnd();
            JObject ser = (JObject)(JObject.Parse(json));
            List<JToken> data = ser.Children().ToList();
            string output = "";

            foreach (JProperty item in data)
            {
                item.CreateReader();
                switch (item.Name)
                {
                    case "response":
                        output += System.Convert.ToString("" + "\r\n");
                        foreach (JObject stream in item.Values())
                        {
                            string game = (string)(stream["game"]); //Display current game whether it ATS or ETS2
                            string name = (string)(stream["name"]); //Display current name
                            string players = (string)(stream["players"]); // Display total players online
                            string max = (string)(stream["maxplayers"]); // Display total max players has set
                            string online = (string)(stream["online"]); // Display current server status
                            string shortname = (string)(stream["shortname"]); //Display the current server shortname  
                            string id = (string)(stream["id"]); // Display the server ID #                                          
                            
                            {

                            }



                            // if servers exist do not add them again
                            if (list.Items.Contains(name + " " + game + " " + players + "/" + max))

                            {

                            }
                            else
                            { // if server doesn't exist add it.
                                
                                list.Items.Add(name + " " + game + " " + players + "/" + max);

                            }
                            // if an event server is online let show a message to them
                            if ((shortname== "EVENT")) 
                            {
                                
                                event_update.Text = name + " " + "Event Server Is Online";
                  
                               
                            }

                           else 
                            { 
                                
                                // if no event servers online let a users know

                                event_update.Text = "No event servers are online";

                            }


                            

                            // Display the total amount of servers truckersMP has even
                            //if offline
                            textme.Text = ("Total Servers Online: ") + " " + list.Items.Count.ToString();
                           
                            // if servers exist do not add them again
                            if (servers.Items.Contains(id)) 
                            {

                            }
                            else
                            { // if server doesn't exist add it.
                               servers.Items.Add(id);
                            }


                        }
                        break;


                }
            }
        }

        private async void servers_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            StreamReader reader = default(StreamReader);

            string url = "https://api.truckersmp.com/v2/servers";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            HttpWebResponse response = (HttpWebResponse)await request.GetResponseAsync(); // (note: await added, method call changed to GetResponseAsync()

            reader = new StreamReader(response.GetResponseStream());
            string responseFromServer = reader.ReadToEnd();
            string json = responseFromServer;
            string rawresp;
            rawresp = reader.ReadToEnd();
            JObject ser = (JObject)(JObject.Parse(json));
            List<JToken> data = ser.Children().ToList();
            string output = "";

            foreach (JProperty item in data)
            {
                item.CreateReader();
                switch (item.Name)
                {
                    case "response":
                        output += System.Convert.ToString("" + "\r\n");
                        foreach (JObject stream in item.Values())
                        {
                            string game = (string)(stream["game"]); //Display current game whether it ATS or ETS2
                            string name = (string)(stream["name"]); //Display current name
                            string players = (string)(stream["players"]); // Display total players online
                            string max = (string)(stream["maxplayers"]); // Display total max players has set
                            string online = (string)(stream["online"]); // Display current server status
                            string shortname = (string)(stream["shortname"]); //Display the current server shortname  
                            string id = (string)(stream["id"]); // Display the server ID #      



                            if (id == servers.SelectedItem.ToString()) // if current selected servers by text
                            {

                                update_me.Text = "Game: " + "  " + game + " " + "Server:" + "  " + name + " " + " Players: " + players + "/" + max + " " + "Online";
                                

                            }


                            


            

                        }
                        break;


                }
            }

        }

        private void auto_refresh_Click(object sender, RoutedEventArgs e)
        {
            
           auto_refresh.Visibility = Visibility.Collapsed;

            auto_refresh.Visibility = Visibility.Visible;
            
            timer.Interval = TimeSpan.FromSeconds(1.0);
           timer.Tick += Timer_Tick;
            //textme.Text = counter.ToString();
            timer.Start();
            //auto_refresh.IsEnabled = false;
         
      

        }
    }
}
  
